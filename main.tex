%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  My documentation report
%  Objetive: Explain what I did and how, so someone can continue with the investigation
%
% Important note:
% Chapter heading images should have a 2:1 width:height ratio,
% e.g. 920px width and 460px height.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
% PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[11pt,fleqn]{book} % Default font size and left-justified equations

\usepackage[top=2cm,bottom=2cm,left=3.2cm,right=3.2cm,headsep=10pt,a4paper]{geometry} % Page margins

\usepackage{xcolor,lipsum} % Required for specifying colors by name
\definecolor{ocre}{RGB}{51,102,0}
\definecolor{lightgray}{RGB}{229,229,229}
% Font Settings
%\usepackage{avant} % Use the Avantgarde font for headings
%\usepackage{times} % Use the Times font for headings
%\usepackage{mathptmx} % Use the Adobe Times Roman as the default text font together with math symbols from the Sym­bol, Chancery and Com­puter Modern fonts

\usepackage{microtype} % Slightly tweak font spacing for aesthetics
\usepackage[utf8]{inputenc} % Required for including letters with accents
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs

\usepackage{booktabs}


% MATHS PACKAGE
\usepackage{amsmath,tikz}
\usetikzlibrary{matrix}
\newcommand*{\horzbar}{\rule[0.05ex]{2.5ex}{0.5pt}}
\usepackage{calc}

% VERBATIM PACKAGE
\usepackage{verbatim}

% Bibliography
\usepackage[style=alphabetic,sorting=nyt,sortcites=true,autopunct=true,babel=hyphen,hyperref=true,abbreviate=false,backref=true,backend=biber]{biblatex}
\addbibresource{bibliography.bib} % BibTeX bibliography file
\defbibheading{bibempty}{}

\input{structure} % Insert the commands.tex file which contains the majority of the structure behind the template

\begin{document}

\let\cleardoublepage\clearpage

%----------------------------------------------------------------------------------------
% TITLE PAGE
%----------------------------------------------------------------------------------------

\begingroup
\thispagestyle{empty}
\AddToShipoutPicture*{\put(0,0){\includegraphics[scale=1]{ime_capa}}} % Image background
\centering
\vspace*{5cm}
%\par\normalfont\fontsize{35}{35}\sffamily\selectfont
\Huge{\textbf{ Plano de Ensino para um Curso de Introdução à Computação }}\\
%{\LARGE }\par % Book title
\vspace*{1cm}
{\LARGE Pedro Bruel \\ Rodrigo Siqueira }\par % Author name
\vfill\par
{\LARGE GEN5711, 1 de Novembro de 2016}\par
\endgroup

%----------------------------------------------------------------------------------------
% TABLE OF CONTENTS
%----------------------------------------------------------------------------------------

\chapterimage{key_header.png} % heading image

\pagestyle{empty} % No headers

\renewcommand{\bibname}{Bibliographie}

\setcounter{tocdepth}{1}
\tableofcontents% Print the table of contents itself

%\cleardoublepage % Forces the first chapter to start on an odd page so it's on the right

\pagestyle{fancy} % Print headers again

%----------------------------------------------------------------------------------------

\chapterimage{key_header.png} % Chapter heading image

\chapter{Introdução}

Este plano de ensino descreve nossa proposta para um curso de introdução à
computação, e foi elaborado durante o curso de Preparação à Docência para
Graduação, GEN5711. Esta Seção descreve brevemente o conceito de Pensamento
Computacional, ou \textit{Computational Thinking}, e o público a quem esse
curso se destina. As demais Seções estão organizadas da seguinte forma: A Seção
\ref{objetivos} apresenta os objetivos que tivemos em mente ao elaborar o
curso. A Seção \ref{conteudo} descreve os tópicos abordados e o cronograma do
curso. A Seção \ref{ensino} apresenta a metodologia de ensino. A Seção
\ref{avalia} apresenta a metodologia de avaliação elaborada. Finalmente, a
Seção \ref{bib} apresenta a bibliografia selecionada para o curso, algumas
referências relacionadas a \textit{Mob Programming}, ao uso de Python
em cursos introdutórios e sobre \textit{Computational Thinking}.

\section{Pensamento Computacional}

Pretendemos neste curso ensinar primeiro os conceitos básicos relacionados à
programação de computadores de uma forma mais distante dos detalhes técnicos e
do funcionamento de computadores, como gerenciamento de memória e sintaxe
específica de linguagens de programação. Para atingir esse objetivo,
utilizaremos o conceito de Pensamento Computacional, ou \textit{Computational
Thinking}.

A ideia do Pensamento Computacional é permitir que os alunos desenvolvam a
capacidade de solucionar problemas computacionais e de programação sem depender
de uma linguagem de programação ou arquitetura de computador, partindo de
conceitos de alto nível, que podem frequentemente ser descritos de forma
simples. Essas soluções devem ser bem estruturadas e escritas, de tal forma que
tanto o computador como um leitor humano possam entender. Os quatro elementos
fundamentais do Pensamento Computacional são:

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.31\textwidth]{images/pc_image_labels.png}
  \caption{Elementos fundamentais do Pensamento Computacional}
\end{figure}

\begin{itemize}
  \item \textbf{Decomposição}: particionamento de um problema em subproblemas
      menores e gerenciáveis
  \item \textbf{Reconhecimento de padrões}: buscar similaridades entre
      problemas já conhecidos
  \item \textbf{Abstração}: identificação das informações relevantes
  \item \textbf{Algoritmo}: desenvolver uma solução estruturada para um
      problema
\end{itemize}

Cursos de Introdução à Computação ou similares costumam ser ministrados nos
primeiros semestres de cursos de Ciências Exatas. Muitos alunos ingressantes
apresentam dificuldades nesses cursos, pois não é comum o ensino de Ciência da
Computação ou programação durante o Ensino Médio. A necessidade de se dominar
uma nova linguagem de programação, aprendendo suas particularidades e
``idiomas'' usuais, pode prejudicar o aprendizado dos alunos e dificultar a
assimilação de conceitos fundamentais. Esses conceitos, considerados os
elementos fundamentais do Pensamento Computacional, serão muito importantes
para cursos mais avançados.

Este curso é fundamentado nos elementos do Pensamento Computacional.
Gostaríamos de facilitar a transição do Ensino Médio ao Ensino Superior no que
diz respeito à assimilação dos conceitos fundamentais para a programação.
Gostaríamos também de despertar nos alunos o interesse pela Ciência da
Computação e seus temas mais avançados.

\section{Público alvo}

A ideia inicial para este curso é que ele seja ministrado no início de cursos
de Bacharelado em Ciências Exatas. Dada a crescente relevância de ferramentas
computacionais para a pesquisa nas mais diversas áreas do conhecimento,
esperamos que cada vez mais cursos do Ensino Superior decidam por preparar seus
alunos para programar. Com muita disposição e boa vontade, imaginamos então que
este curso poderá ser também, no futuro, ministrado em cursos das áreas de
Humanas e Exatas.

%------------------------------------------------------------------------------
\chapterimage{key_header.png}
\chapter{Objetivos}
\label{objetivos}

Elaboramos três objetivos que pretendemos atingir durante o curso.  Os
objetivos consideram o aprendizado do Pensamento Computacional, a escrita de
programas bem estruturados na linguagem Python e os aspectos colaborativos da
programação como os conceitos fundamentais para uma introdução à computação.

\section{Pensamento Computacional}

A primeira etapa do curso aborda o Pensamento Computacional, trabalhando com os
temas ``Abstração'', ``Reconhecimento e Geração de Padrões'', ``Decomposição de
Dados, Processos e Problemas'' e ``Desenvolvimento de Algoritmos''. Os demais
temas do Pensamento Computacional, como ``Paralelização'', poderão ser
discutidos de forma superficial, mas sua utilização ainda é muito técnica
para um curso introdutório.

\section{Introdução à Programação em Python}

Espera-se ao final deste curso que os alunos sejam capazes de criar programas
que apliquem os conceitos do Pensamento Computacional, expressando soluções
para problemas por meio da construção de código na linguagem Python. A
linguagem Python possui uma sintaxe e um modelo de programação bastante
simples.  Usando Python o aluno não precisa se preocupar com gerenciamento de
memória, por exemplo. A linguagem tem sido cada vez mais utilizada em cursos
introdutórios de Ciência da Computação.

Os alunos deverão aprender a desenvolver programas ``corretos'', isto é, que
resolvem um problema, legíveis tanto pelo computador como por leitores humanos,
e simples, isto é, não fazem mais do que o necessário e não têm um fluxo
``labiríntico'' de execução.

\section {Programação como uma Atividade Colaborativa}

Hoje é evidente que o processo de desenvolvimento de software não é uma
atividade solitária e que é preciso trabalhar em conjunto com outros
desenvolvedores. O curso deve despertar nos alunos a importância e o interesse
de se programar de forma colaborativa. Além disso, o curso deve introduzir
metodologias que facilitem a coordenação de atividades colaborativas, como o
\textit{Mob Programming} e o \textit{Coding Dojo}.

% -----------------------------------------------------------------------------
\chapterimage{key_header.png}
\chapter{Conteúdo}
\label{conteudo}

Esta Seção apresenta os conteúdos de Pensamento Computacional e Programação
em Python abordados durante o curso. A Tabela \ref{cronograma} descreve
os momentos ideias para abordar cada conteúdo.

\section{Tópicos}

\subsection{Pensamento Computacional}

\begin{itemize}
  \item \textbf{Abstração}: representar um problema através de suas as informações mais relevantes
  \item \textbf{Desenvolvimento de Algoritmos}: desenvolver uma solução estruturada para resolver um problema
  \item \textbf{Automatização}: ``tradução'' de um algoritmo para a linguagem de computadores
  \item \textbf{Coleta e Análise de Dados}: identificar e coletar dados relevantes para a solução de um problema
  \item \textbf{Decomposição}: divisão em subproblemas
  \item \textbf{Paralelização}: fazer com que pequenas tarefas possam ser
        executadas em paralelo
  \item \textbf{Reconhecimento de Padrões}: encontrar tendências, repetições e regularidades em conjuntos de dados
  \item \textbf{Generalização de Padrões}: generalizar os padrões encontrados, possivelmente fazer predições
  \item \textbf{Simulação}: desenvolver modelos que imitem o mundo real
  \item \textbf{Trabalho Colaborativo}: programação é um processo colaborativo
\end{itemize}

\subsection{Programação em Python}

\begin{itemize}
    \item Condicionais
    \item Laços
    \item Funções e Recursão
    \item Listas e Matrizes
    \item Introdução à Programação Orientada a Objetos
    \item Modularização
    \item Ferramentas: Bibliotecas
\end{itemize}

\newpage
\section{Cronograma}

\begin{table}[htpb]
\centering
\small
\begin{tabular}{@{}ll@{}}
    \toprule
    Semanas & Conteúdo \\ \midrule
    \addlinespace[.5em]
    1: Introdução & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Por que programar é importante para você?
            \item O cotidiano, a programação e os paradigmas
        \end{itemize}%
    \end{minipage} \\
    \addlinespace[1.5em]
    2: Decomposição & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Divisão e conquista
            \item Aplicação dos conceitos: ``dinâmica'' em sala de aula
        \end{itemize}
    \end{minipage} \\
    \addlinespace[1.5em]
    3: Padrões & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Entender o que são padrões, encontrá-los e extraí-los
            \item Padrões em diferentes problemas
        \end{itemize}
    \end{minipage}     \\
    \addlinespace[1.5em]
   6: Abstrações & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Trabalhando com abstrações
            \item Usando modelos
        \end{itemize}
    \end{minipage}     \\
    \addlinespace[1.5em]
    5: Algoritmos & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item O que é um algoritmo? Para que serve?
            \item Montando planos, pseudocódigo e fluxograma
        \end{itemize}
    \end{minipage}     \\
    \addlinespace[1.5em]
    6: Avaliação 1& \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Apresentação do problema e explicação das regras
            \item Trabalho em sala e entrega da avaliação
        \end{itemize}
    \end{minipage}     \\
    \addlinespace[1.5em]
    7: Variáveis, Condicionais, EP 1&  \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Linguagem Python e seu ambiente
            \item Apresentação de variáveis e condicionais
            \item EP 1
        \end{itemize}
    \end{minipage}     \\
    \addlinespace[1.5em]
    8: Laços, EP2 & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Apresentando o conceito de repetição
            \item Usando condicionais e repetição
            \item EP 2
        \end{itemize}
    \end{minipage}        \\
    \addlinespace[1.5em]
    9: Funções, EP3 & \begin{minipage}[t]{.6\textwidth}%
        \begin{itemize}
            \item Apresentação de funções
            \item Usando condicionais, repetições e funções
            \item EP 3
        \end{itemize}
    \end{minipage}    \\
    \addlinespace[1.5em]
10: Boas práticas, E.6 &  \begin{minipage}[t]{.6\textwidth}%
    \begin{itemize}
        \item Organizando e documentando o código
        \item Boas práticas de programação
        \item Proposta dos projetos
        \item EP 4
    \end{itemize}
    \end{minipage}        \\
    \addlinespace[1.5em]
11: Projeto & \begin{minipage}[t]{.6\textwidth}%
    \begin{itemize}
        \item Monitores e professores auxiliam no projeto
        \item EP 5
    \end{itemize}
\end{minipage}        \\
    \addlinespace[1.5em]
12: Projeto & \begin{minipage}[t]{.6\textwidth}%
    \begin{itemize}
        \item Trabalho no projeto
    \end{itemize}
\end{minipage}         \\
    \addlinespace[1.5em]
13: Projeto & \begin{minipage}[t]{.6\textwidth}%
    \begin{itemize}
        \item Trabalho no projeto
        \item Entrega do Projeto
    \end{itemize}
\end{minipage}         \\
\addlinespace[.5em]
\bottomrule
\end{tabular}
\caption{Cronograma para o Curso}
\label{cronograma}
\end{table}

%------------------------------------------------------------------------------
\chapterimage{key_header.png}
\chapter{Metodologia de Ensino}
\label{ensino}
% ------------------------------------------------------

O curso é composto de duas partes. A primeira parte introduz o Pensamento
Computacional e seus fundamentos. A segunda parte introduz a linguagem Python,
seu ambiente de desenvolvimento e os conceitos necessários para implementar
programas simples. Utilizaremos três estratégias para compor nossa metodologia
de ensino: aulas expositivas, discussões e Exercícios Participativos, ou EPs,
que serão realizados dentro e fora da sala de aula.

\section{Aulas Expositivas}

Para as aulas expositivas utilizaremos diferentes recursos visuais como o
quadro e apresentações em \textit{slides}. As aulas expositivas devem
misturar-se com as discussões, pois é desejável complementar cada exposição com
uma discussão durante a aula.

\section{Discussões}

O professor deverá incentivar as discussões em sala de aula, que deverão
ocorrer de forma planejada e guiada pelo professor, com o objetivo de motivar a
participação do aluno na resolução de problemas apresentados em aula.

\section{Exercícios Participativos}

São exercícios apresentados em sala de aula, que podem ser resolvidos durante a
própria aula ou entregues como trabalhos. Se o exercício for feito em casa, os
alunos poderão escolher se querem participar de um grupo ou resolver o
exercício sozinhos.

Se o exercício for resolvido em sala adaptaremos o método ágil ``Mob
Programming''. A ideia é dividir a sala em grupos, de 10 alunos por exemplo.
Em cada grupo, apenas uma pessoa por vez será responsável por escrever a
solução num pedaço de papel ou computador. Os demais membros do grupo serão
responsáveis por instruir o escritor, ou ``piloto'', a escrever a solução. O
``piloto'' não participa da solução. Após um determinado tempo, 10 minutos por
exemplo, troca-se o ``piloto''. Todos devem ``pilotar'' ao menos uma vez
durante a solução do problema.


%------------------------------------------------------------------------------
\chapterimage{key_header.png}

\chapter{Metodologia de Avaliação}
\label{avalia}

% -----------------------------------------------------------------------------

O método de avaliação procura explorar os três objetivos do curso, descritos na
seção \ref{objetivos}. É fundamental para esse curso que os alunos possam
escolher como serão avaliados, dentre algumas formas de avaliação. A nota final
será composta por dois tipos de avaliação: um trabalho feito em equipe e um
conjunto de Exercícios Participativos, ou EPs. Com essa composição de
avaliações, buscamos fornecer ao aluno uma maior flexibilidade, dado que
entendemos que cada aluno costuma ter um tipo de avaliação no qual se sai
melhor.

\section{Avaliação dos Alunos}

A primeira avaliação consiste em um trabalho em grupo, que busca mostrar aos
alunos que o desenvolvimento de \textit{software} é um processo complexo, que é
comumente realizado de forma colaborativa. A ideia deste trabalho é fornecer um
problema no qual os alunos possam exercitar sua capacidade de trabalhar em
grupo.  A avaliação é divida em duas etapas de dois dias de duração. Na
primeira etapa os alunos começam a trabalhar na solução do problema. Na segunda
os alunos devem apresentar uma solução estruturada, usando ``pseudo código'' ou
``português estruturado''.

Nesta atividade o curso deverá utilizar uma adaptação do método ágil ``Mob
Programming''. A classe será dividida em grupos, de 10 alunos exemplo.  A ideia
principal é que apenas um membro do grupo, o ``piloto'', escreverá a solução
para o problema. Os demais membros deverão tentar resolver o problema e
instruir o ``piloto'' na escrita da solução.

Após um determinado período, 10 minutos por exemplo, o professor deverá
sinalizar a troca de ``piloto'. Isso pode ser feito batendo-se num gongo, por
exemplo. O objetivo desta prática é fazer com que todos os alunos pensem em
conjunto e entendam que precisam colaborar. Note que a função do gongo não é
assustar o aluno, outro instrumento poderia ser utilizado para deixar claro que
é hora de trocar o ``piloto''. Todos os membros do grupo deverão assumir o
papel do ``piloto'' ao menos uma vez durante a solução do exercício.

A segunda forma de avaliação consiste numa série de EPs com correção
automatizada.  O aluno pode escolher trabalhar em dupla, trio ou
individualmente. Esses EPs são problemas pré-definidos e um pequeno conjunto de
testes entregues ao aluno. Usando os testes pode verificar se seu programa está
funcionando como o esperado. Quando o aluno se sentir confortável, poderá
enviar seu código para correção automatizada, onde um número maior de testes
será feito.  O aluno receberá a nota referente ao seu programa, e poderá enviar
quantas tentativas quiser dentro do prazo estipulado para a entrega.

Finalmente será proposto um projeto final de programação, onde o aluno
trabalhará junto com outros membros, gerando um código final. Para tornar o
trabalho mais colaborativo e fornecer ao aluno a possibilidade de tirar dúvidas
pessoalmente com professores e monitores, o trabalho final será desenvolvido
durante as aulas. Espera-se que os alunos trabalhem também fora das aulas,
aproveitando para tirar dúvidas e conversar com seus colegas durante a aula.

O cálculo da nota final desconsidera a menor nota do aluno, e segue o padrão
de avaliação, isto é, conceitos ou notas numéricas, adotado pela instituição
onde será dado o curso.

\section{Avaliação do Curso, ou Retrospectiva}

Após cada parte do curso serão realizadas avaliações referentes ao curso.
Essas avaliações serão feitas através da resposta de questionários e discussões
em sala de aula. A ideia é fornecer aos professores e monitores informações
sobre as opiniões dos alunos sobre curso até o momento da retrospectiva.  Com
essas informações deverá ser possível compreender como os alunos se sentem em
relação aos conteúdos discutidos, sua facilidade em aprender e sua relação com
os colegas, professores e monitores. Dessa forma será possível entender a
efetividade e o recebimento do curso entre os alunos, e modificar o que for
necessário durante a duração do curso. Além disso, será possível melhorar o
curso para próximas turmas, pois essas informações podem ser aplicadas em
instâncias futuras do curso.

%------------------------------------------------------------------------------
\chapterimage{key_header.png}
\chapter{Bibliografia}
\label{bib}
% -----------------------------------------------------------------------------

O aluno terá acesso ao material desenvolvido durante o curso, na forma
de \textit{slides} e notas de aula. Procuraremos utilizar
material dos seguintes livros, sites e referências:

\begin{itemize}
    \item Introdução à Computação e Programação
        usando Python, John V. Guttag
    \item Introdução à Programação Com Python:
        Algoritmos e Lógica de Programação
        Para Iniciantes, Nilo Ney Coutinho
    \item How to Think Like a Computer Scientist:
        Interactive Edition, Jeffrey Elkner, Allen B. Downey,
        e Chris Meyers
    \item Aprendendo com Python:
        Edição interativa (Tradução do livro acima)
    \item Ranum, David, et al. ``Successful approaches to teaching
        introductory computer science courses with python'' ACM SIGCSE
        Bulletin. Vol. 38. No. 1. ACM, 2006
    \item Fangohr, Hans. ``A comparison of C, MATLAB, and Python
        as teaching languages in engineering'' ICCS. Springer Berlin
        Heidelberg, 2004.
    \item Guo, Philip. ``Python is now the most popular introductory
        teaching language at top us universities'' BLOG@ CACM, July (2014)
    \item \footnotesize{\url{google.com/edu/resources/programs/exploring-computational-thinking} [Acessado em 1/11/16]}
    \item \url{bbc.co.uk/education/guides/zp92mp3/revision} [Acessado em 1/11/16]
    \item \url{interactivepython.org/courselib/static/thinkcspy/index.html} [Acessado em 1/11/16]
    \item \url{panda.ime.usp.br/pensepy/static/pensepy} [Acessado em 1/11/16]
    \item \url{mobprogramming.org} [Acessado em 1/11/16]
\end{itemize}

\end{document}
